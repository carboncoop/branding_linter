# Branding Linter
This repo contains a few branding linters to prevent developers committing the wrong branding for some things.

For example, PowerShaper is the correct branding for the service - not Powershaper or Power Shaper.


To add a new variation to catch, open the `pre-commit-hooks.yaml` file and edit the appropriate `entry key`, adding a pipe `|` and the new variation:

 `entry: "Power Shaper|Powershaper|new variation"`

